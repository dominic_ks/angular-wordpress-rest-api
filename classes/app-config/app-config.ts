export class AppConfig {
  siteUrl?: string;
  contentUrl?: string;
  appVersion?: string;
  cacheRules?: {
    global: boolean;
    alwaysCache: string[];
    neverCache: string[];
  };
  cacheExpiryService?: {
    url: string;
    nameSpace: string;
    frequency: number;
  }
}
