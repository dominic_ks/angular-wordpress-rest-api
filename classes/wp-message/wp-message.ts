export class WpMessage {
  title?: {
    rendered: string;
  };
  type: string;
  message: string;
}
