export class WpQuery {
  postType?: string;
  id?: number;
  pageNumber?: number;
  args?: Object;
  skipCache?: boolean;
}
