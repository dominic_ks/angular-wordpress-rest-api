import { Injectable } from '@angular/core';

import { WpServerService } from '../wp-server/wp-server.service';

import { WpQuery } from '../../classes/wp-query/wp-query';

@Injectable({
  providedIn: 'root'
})
export class WpDeleteService {

  constructor(
    private wpServerService: WpServerService,
  ) { }

  deleteObject( object: WpQuery ) {

    let url = object.postType + '/' + object.id;

    if( object.postType === 'media' ) {
      url += '?force=true';
    }

    return this.wpServerService.deleteObject( url );

  }

}
