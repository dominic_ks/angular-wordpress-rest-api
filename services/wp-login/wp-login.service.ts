import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable, empty } from 'rxjs';
import { catchError, map, tap, mergeMap } from 'rxjs/operators';

import { WpServerService } from '../wp-server/wp-server.service';

@Injectable({
  providedIn: 'root'
})
export class WpLoginService {

  private loginStatus = new BehaviorSubject( 'unknown' );
  currentLoginStatus = this.loginStatus.asObservable();

  public userToken: string;

  constructor(
    private wpServerService: WpServerService,
  ) { }

  updateLoginStatus( status: string ): void {
    this.loginStatus.next( status );
  }

  checkLoginStatus(): void {

    let token = localStorage.getItem( 'userToken' );

    if( typeof( token ) !== 'string' ) {
      return this.logout();
    }

    this.wpServerService.validateToken().subscribe(

      resp => {

        if( resp['data']['status'] == 200 ) {
          return this.updateLoginStatus( 'logged-in' );
        }

        this.logout();

      },

      error => {
        this.logout();
      },

    )

  }

  login( data ): Observable<{}> {
    return this.wpServerService.getToken( data ).pipe(
      tap( resp => {
        this.processRequestToken( resp );
      })
    );
  }

  processRequestToken( response: Object ) {

    let status: string = 'not-logged-in';

    if( typeof( response ) !== 'undefined' && typeof( response['token'] ) !== 'undefined' ) {
      localStorage.setItem( 'userToken' , response['token'] );
      status = 'logged-in';
    }

    return this.updateLoginStatus( status );

  }

  resetPassword( userData: Object ) {
    return this.wpServerService.createObject( '/reset-password/' , userData , '/wig/v1' );
  }

  setNewPassword( userData: Object ) {
    return this.wpServerService.createObject( '/set-new-password/' , userData , '/wig/v1' );
  }

  logout():void {
    this.updateLoginStatus( 'not-logged-in' );
    localStorage.clear();
  }

}
