import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';

import { WpMessagesService } from './wp-messages.service';

describe('WpMessagesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
    ],
  }));

  it('should be created', () => {
    const service: WpMessagesService = TestBed.get(WpMessagesService);
    expect(service).toBeTruthy();
  });
});
