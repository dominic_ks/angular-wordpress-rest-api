import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';

import { WpQueryService } from './wp-query.service';

describe('WpQueryService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
    ],
  }));

  it('should be created', () => {
    const service: WpQueryService = TestBed.get(WpQueryService);
    expect(service).toBeTruthy();
  });
});
