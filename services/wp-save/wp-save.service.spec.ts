import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';

import { WpSaveService } from './wp-save.service';

describe('WpSaveService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
    ],
  }));

  it('should be created', () => {
    const service: WpSaveService = TestBed.get(WpSaveService);
    expect(service).toBeTruthy();
  });
});
