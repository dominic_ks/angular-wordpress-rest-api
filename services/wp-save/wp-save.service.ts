import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { WpServerService } from '../wp-server/wp-server.service';

import { WpObject } from '../../classes/wp-object/wp-object';

@Injectable({
  providedIn: 'root'
})

export class WpSaveService {

  constructor(
    private wpServerService: WpServerService,
  ) { }

  saveObject( route: string , object: WpObject , sendArgs: string[] = [] ): Observable<WpObject> {

    object = ( sendArgs.length === 0 ? object : this.parseObject( object , sendArgs ));

    if( typeof( object.id ) === 'undefined' || object.id === 0 ) {
      return this.createObject( route , object );
    }

    return this.updateObject( route , object );

  }

  updateObject( route: string , object: WpObject ): Observable<WpObject> {

    let id = object.id;
    delete object.id;

    return this.wpServerService.updateObject( route , id , object );

  }

  createObject( route: string , object: WpObject ): Observable<WpObject> {

    if( typeof( object.id ) !== 'undefined' ) {
      delete object.id;
    }

    return this.wpServerService.createObject( route , object );

  }

  parseObject( object: WpObject , sendArgs: string[] ): WpObject {

    let returnObject: WpObject = {}

    for( var key in object ) {

      if( sendArgs.indexOf( key ) === -1 ) {
        continue;
      }

      returnObject[ key ] = ( typeof( object[ key ] ) === 'object' ? JSON.stringify( object[ key ] ) : object[ key ] );

    }

    return returnObject;

  }

}
