import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap, mergeMap } from 'rxjs/operators';

import { WpConfigService } from '../wp-config/wp-config.service';
import { WpMessagesService } from '../wp-messages/wp-messages.service';

import { AppConfig } from '../../classes/app-config/app-config';
import { WpObject } from '../../classes/wp-object/wp-object';

@Injectable({
  providedIn: 'root'
})
export class WpServerService {

  httpOptions: Object;
  apiUrl: string;

  constructor(
    public http: HttpClient,
    private wpConfigService: WpConfigService,
    private wpMessagesService: WpMessagesService,
  ) {

    this.wpConfigService.currentAppConfig.subscribe(
      resp => {

        if( resp === {} ) {
          return;
        }

        this.apiUrl = resp['siteUrl'];

      }
    );

  }

  getHttpHeaders( observe = false , auth = true , extraHeaders = {} ) {

    let headers = Object.assign({
      'Content-Type': 'application/json',
    }, extraHeaders );

    let token = localStorage.getItem( 'userToken' );

    if( typeof( token ) === 'string' ) {
      headers['Authorization'] = 'Bearer ' + localStorage.getItem( 'userToken' );
    }

    this.httpOptions = {
      headers: new HttpHeaders( headers ),
    }

    if( observe ) {
      this.httpOptions['observe'] = 'response';
    }

    return this.httpOptions;

  }

  //get a token
  getToken( data: Object ) {
    return this.http.post( this.apiUrl + '/jwt-auth/v1/token/' , data , this.getHttpHeaders() ).pipe(
      catchError( this.handleError<any>('getToken') )
    );
  }

  //validate token
  validateToken() {
    return this.http.post( this.apiUrl + '/jwt-auth/v1/token/validate/ ' , {} , this.getHttpHeaders() );
  }

  executeWpQuery( url: string , nameSpace?: string ) {

    let ns = ( typeof( nameSpace ) === 'undefined' ? '/wp/v2/' : nameSpace );

    return this.http.get<HttpResponse<any>>( this.apiUrl + ns + url , this.getHttpHeaders( true )).pipe(
      catchError( this.handleError<any>('executeWpQuery') )
    );

  }

  //creare a new services
  createObject( route: string , data: WpObject , nameSpace?: string ) {

    let ns = ( typeof( nameSpace ) === 'undefined' ? '/wp/v2/' : nameSpace );

    return this.http.post<WpObject>( this.apiUrl + ns + route , data , this.getHttpHeaders() ).pipe(
      catchError( this.handleError<any>('createObject') )
    );

  }

  //update a service
  updateObject( route: string , id: number , data: WpObject , nameSpace?: string ) {

    let ns = ( typeof( nameSpace ) === 'undefined' ? '/wp/v2/' : nameSpace );

    return this.http.post<WpObject>( this.apiUrl + ns + route + '/' + id , data , this.getHttpHeaders() ).pipe(
      catchError( this.handleError<any>('updateObject') )
    );

  }

  //upload a file
  uploadFile( data: Object , filename: string, nameSpace?: string ) {

    return this.http.post<WpObject>( this.apiUrl + '/wp/v2/media' , data , this.getHttpHeaders( false , true , {
      'Content-Type' : 'image/jpeg',
    })).pipe(
      catchError( this.handleError<any>('uploadFile') )
    );

  }

  //delete an object
  deleteObject( route: string , nameSpace?: string ) {

    let ns = ( typeof( nameSpace === 'undefined' ) ? '/wp/v2/' : nameSpace );

    return this.http.delete<WpObject>( this.apiUrl + ns + route , this.getHttpHeaders() ).pipe(
      catchError( this.handleError<any>('deleteObject') )
    );

  }

  //handle errors
  private handleError<T> ( operation = 'operation' , result?: T ) {
    return( error: any ): Observable<T> => {

      let message = `${operation} failed: ${error.message}`;

      if( typeof( error['error'] ) !== 'undefined' && typeof( error['error']['message'] ) !== 'undefined' ) {
        message = `${error['error']['message']}`
      }

      this.wpMessagesService.broadcastMessage({
        title: {
          rendered: 'Error',
        },
        type: 'error',
        message: message,
      });

      // Let the app keep running by returning an empty result.
      return of( result as T );

    };
  }

}
