import { Injectable } from '@angular/core';

import { WpServerService } from '../wp-server/wp-server.service';

@Injectable({
  providedIn: 'root'
})
export class WpUploadService {

  constructor(
    private wpServerService: WpServerService,
  ) { }

  uploadFile( data: Object ) {
    let fileName = data['file']['name'];
    return this.wpServerService.uploadFile( data , fileName );
  }

}
