import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable, empty } from 'rxjs';
import { catchError, map, tap, mergeMap } from 'rxjs/operators';

import { WpQueryService } from '../wp-query/wp-query.service';
import { WpLoginService } from '../wp-login/wp-login.service';

import { WpUser } from '../../classes/wp-user/wp-user';

@Injectable({
  providedIn: 'root'
})
export class WpUserService {

  private user = new BehaviorSubject({});
  currentUser = this.user.asObservable();

  constructor(
    private wpQueryService: WpQueryService,
    private wpLoginService: WpLoginService,
  ) {

    this.wpLoginService.currentLoginStatus.subscribe(
      resp => {

        if( resp !== 'logged-in' ) {
          this.updateCurrentUser({
            roles: [],
          })
        }

      }
    );

  }

  updateCurrentUser( user: WpUser ): void {
    this.user.next( user );
  }

  getCurrentUser() {

    this.wpQueryService.getPosts({
      postType: 'users/me',
      args: {
        context: 'edit',
      },
    }).subscribe(
      resp => {
        this.updateCurrentUser( resp['body'] );
      }
    );

  }

}
